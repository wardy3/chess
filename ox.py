"""Try naughts and crosses."""
import itertools
from enum import StrEnum

import pygame
import typer
from pygame.font import Font

__DESCRIPTION__ = __doc__.partition("\n")[0].strip()

# Define constants for player symbols
PLAYER_X = "X"
PLAYER_O = "O"

WIDTH = 600
HEIGHT = 600

FONT_SIZE = 100


BLACK = (255, 255, 255)
WHITE = (0, 0, 0)

FLASH_INTERVAL = 500  # in milliseconds


# -----
Board = list[list[str]]


class WinState(StrEnum):
    ROW1 = "ROW1"
    ROW2 = "ROW2"
    ROW3 = "ROW3"
    COL1 = "COL1"
    COL2 = "COL2"
    COL3 = "COL3"
    DIA1 = "DIA1"
    DIA2 = "DIA2"
    NOWIN = "NOWIN"

    def is_winner(self, *, row: int, col: int) -> bool:
        # sourcery skip: assign-if-exp, reintroduce-else
        """Check if the given position is part of the winning state."""
        if self.startswith("ROW"):
            return row == int(self[-1]) - 1
        if self.startswith("COL"):
            return col == int(self[-1]) - 1
        if self == WinState.DIA1:
            return row == col
        if self == WinState.DIA2:
            return row == 2 - col

        return False

    @property
    def won(self) -> bool:
        return self is not WinState.NOWIN


def main() -> None:
    """Play tic tac toe."""
    screen, font = initialize_pygame()

    # Initialize the game board
    board = initialize_board()

    current_player = PLAYER_X
    win_state = WinState.NOWIN

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                raise typer.Exit()

            if win_state.won:
                flash_symbols(screen, board=board, font=font, win_state=win_state)

                # restart game
                if event.type == pygame.MOUSEBUTTONDOWN:
                    raise typer.Exit()

            else:
                if event.type == pygame.MOUSEBUTTONDOWN:
                    row, col = get_clicked_position(pygame.mouse.get_pos())

                    update_board(board, row=row, col=col, player=current_player)
                    win_state = check_win(board, player=current_player)

                    current_player = switch_player(current_player)

                # Draw the board
                draw_board(screen, board=board, font=font)

                pygame.display.flip()


def initialize_pygame():
    """Initialize Pygame and set up the window."""
    pygame.init()
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("Tic Tac Toe")

    font = pygame.font.Font(None, FONT_SIZE)

    return screen, font


def initialize_board() -> Board:
    """Initialize the game board."""
    return [["" for _ in range(3)] for _ in range(3)]


def get_clicked_position(position: tuple[int, int]) -> tuple[int, int]:
    """Handle mouse click and return the clicked square position."""
    x, y = position
    return y // (HEIGHT // 3), x // (WIDTH // 3)


def update_board(board: Board, *, row: int, col: int, player: str) -> None:
    """Update the game board by placing X or O in the clicked position."""
    if board[row][col] == "":
        board[row][col] = player


def switch_player(current_player: str) -> str:
    """Switch to the other player."""
    return PLAYER_O if current_player == PLAYER_X else PLAYER_X


def draw_board(screen: pygame.Surface, *, board: Board, font: Font) -> None:
    """Draw the Tic Tac Toe board on the screen."""
    draw_lines(screen)

    # Draw symbols
    for i, j in itertools.product(range(3), range(3)):
        if cell_value := board[i][j]:
            text = font.render(cell_value, True, BLACK)
            text_rect = text.get_rect(
                center=(
                    j * (WIDTH // 3) + (WIDTH // 6),
                    i * (HEIGHT // 3) + (HEIGHT // 6),
                )
            )
            screen.blit(text, text_rect)


def check_win(board: Board, *, player: str) -> WinState:
    """Check if the current player has won."""
    # Check rows and columns
    for i in range(3):
        if all(board[i][j] == player for j in range(3)):
            return WinState(f"ROW{i + 1}")

        if all(board[j][i] == player for j in range(3)):
            return WinState(f"COL{i + 1}")

    # Check diagonals
    if all(board[i][i] == player for i in range(3)):
        return WinState.DIA1

    if all(board[i][2 - i] == player for i in range(3)):
        return WinState.DIA2

    return WinState.NOWIN


def draw_lines(screen):
    line_width = 15
    for i in range(1, 3):
        pygame.draw.line(
            screen,
            BLACK,
            (0, i * (HEIGHT // 3)),
            (WIDTH, i * (HEIGHT // 3)),
            line_width,
        )
        pygame.draw.line(
            screen, BLACK, (i * (WIDTH // 3), 0), (i * (WIDTH // 3), HEIGHT), line_width
        )


def flash_symbols(
    screen: pygame.Surface, *, board: Board, font: Font, win_state: WinState
) -> None:
    """Flash the winning row/column/diagonal symbols for a short duration."""
    for colour in (WHITE, BLACK):
        for i, j in itertools.product(range(3), range(3)):
            if win_state.is_winner(row=i, col=j):
                print(f"setting {i},{j} to {colour}")
                text_surface = font.render(board[i][j], True, colour)
                text_rect = text_surface.get_rect(
                    center=(
                        j * (WIDTH // 3) + (WIDTH // 6),
                        i * (HEIGHT // 3) + (HEIGHT // 6),
                    )
                )
                screen.blit(text_surface, text_rect)

        pygame.display.flip()

        # Add a short delay between flash iterations
        pygame.time.delay(FLASH_INTERVAL)


if __name__ == "__main__":  # pragma: no cover
    _app = typer.Typer(add_completion=False)
    _app.command(help=__DESCRIPTION__)(main)
    _app()
